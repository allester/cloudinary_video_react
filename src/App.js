import VideoPlayer from './components/VideoPlayer';
import './App.css';

function App() {
  return (
    <main className="main">
      <div className="container">
        <h1 className='text-center'>Cloudinary Video Player</h1>
        <VideoPlayer 
          width={960}
          height={540} 
        />
      </div>
    </main>
  );
}

export default App;
