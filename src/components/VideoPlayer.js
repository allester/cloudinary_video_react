import React, { useEffect, useRef } from 'react'

const VideoPlayer = (props) => {
    const { width, height } = props
    const cloudinaryRef = useRef()
    const videoRef = useRef()
    useEffect(() => {
        if ( cloudinaryRef.current ) return;
        cloudinaryRef.current = window.cloudinary
        cloudinaryRef.current.videoPlayer(videoRef.current, {
            cloud_name: `${process.env.REACT_APP_CLOUDINARY_CLOUD_NAME}`
        })
    }, [])

    return (
        <video 
            ref={videoRef} 
            data-cld-public-id={`${process.env.REACT_APP_VIDEO_PUBLIC_ID}`} 
            width={width} 
            height={height} 
            controls 
            
            {...props}
        />
    )
}

export default VideoPlayer