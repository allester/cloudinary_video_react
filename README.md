# Simple React App Showcasing Cloudinary Video

This is a simple react application showcasing some of the capabilities of Cloudinary video and consequently serving a video hosted on Cloudinary

## Setup

### Install dependencies

Clone the repository to your local environment

cd into the cloned directory

```bash
npm install
```

### Populate environment variables

Rename **.env.example** to **.env** and edit the values by adding your Cloudinary cloud name as well as the public ID for the video you would like to see displayed

Run the command below to start the server

```bash
npm start
```